package org.fasttrackit.magentotauproject;
import ClassData.Cart;
import ClassData.Products;
import io.qameta.allure.Feature;
import org.testng.annotations.*;
import static com.codeborne.selenide.Condition.*;
import ClassData.Footer;
import static com.codeborne.selenide.Selenide.*;
public class MainPageTest {
    MainPage mainPage = new MainPage();
    Footer footer = new Footer();
    Products products = new Products();
    Cart cart = new Cart();

    @BeforeMethod
    public void setUp() {
        open("http://testfasttrackit.info/magento-test/");
    }

    @Feature("Search and validate one product.")
    @Test
    public void i_can_search_for_a_product() {
        mainPage.searchProduct("Men Clothes");
        $(".search-button").click();
        $(".product-name a[title*=Green]").shouldBe(visible).shouldHave(text("Sapuniera -Green"));
        mainPage.resetPage();
    }

    @Feature("Login")
    @Test(description = "I can login with valid credentials.")
    public void login_with_valid_credentials() {
        mainPage.account_button();
        $("div#header-account li.first a[title*=Account]").shouldBe(visible);
        mainPage.my_account();
        mainPage.email_address("Gicu@yahoo.com");
        mainPage.user_password("Gigel2000");
        mainPage.clickOnLoginButton();
        mainPage.verifyWelcomeMessageByText("WELCOME, GICU R SERBAN!");
        mainPage.userLogOut();
        mainPage.resetPage();
    }

    @Feature("Login")
    @Test(description = "I receive a error message when logging with wrong password.")
    public void invalid_password_error_message () {
        mainPage.account_button();
        $("div#header-account li.first a[title*=Account]").shouldBe(visible);
        mainPage.my_account();
        mainPage.email_address("Gicu@yahoo.com");
        mainPage.user_password("Gigel20001");
        mainPage.clickOnLoginButton();
        mainPage.verifyErrorMessageByText("Invalid login or password.");
        mainPage.resetPage();
    }

    @Feature("Login")
    @Test(description = "I receive a error message when logging with wrong user.")
    public void invalid_user_error_message(){
        mainPage.account_button();
        $("div#header-account li.first a[title*=Account]").shouldBe(visible);
        mainPage.my_account();
        mainPage.email_address("Gicu-Serban@yahoo.com");
        mainPage.user_password("Gigel2000");
        mainPage.clickOnLoginButton();
        mainPage.verifyErrorMessageByText("Invalid login or password.");
        mainPage.resetPage();
    }

    @Feature("Homepage")
    @Test(description = "I am expecting a welcome message here.")
    public void expect_welcome_message_as_guest(){
        mainPage.verifyGuestMessage("Welcome ");
    }

    @Feature("Homepage")
    @Test(description = "I can modify the language on webpage.")
    public void change_language_on_page(){
        mainPage.languageSelection();
    }

    @Feature("Footer")
    @Test(description = "I verify footer message here.")
    public void test_footer_content_message(){
        footer.verifyFooterContent();
    }

    @Feature("Products")
    @Test(description = "I can access Lafayette Convertible Dress from New Products list.")
    public void i_can_access_new_products_items(){
        products.getForthProduct("Lafayette Convertible Dress");
        mainPage.resetPage();
    }

    @Feature("Products")
    @Test(description = "I can add a product to compare list.")
    public void i_can_add_to_compare_list(){
        products.getFifthProduct();
        products.verifyProductInShoppingCartByName("The product Linen Blazer has been" +
                " added to comparison list.");
        mainPage.resetPage();
    }

    @Feature("Products")
    @Test(description = "I can add a product to the cart.")
    public void add_a_product_to_cart(){
        products.getFirstProduct();
        products.verifyProductInShoppingCartByName("Baby girl cardigan  was added to your shopping cart.");
        mainPage.resetPage();
    }

    @Feature("Products")
    @Test(description = "I can add a product to my wishlist.")
    public void add_a_product_to_wish_list(){
        mainPage.account_button();
        $("div#header-account li.first a[title*=Account]").shouldBe(visible);
        mainPage.my_account();
        mainPage.email_address("Gicu@yahoo.com");
        mainPage.user_password("Gigel2000");
        mainPage.clickOnLoginButton();
        mainPage.resetPage();
        products.getSecondProduct();
        products.verifyProductInShoppingCartByName("Unicorn Jewelry Set has been added to your wishlist." +
                " Click here to continue shopping.");
        cart.removeItemFromWishlist();
        mainPage.userLogOut();
        mainPage.resetPage();
    }

    @Feature("Products")
    @Test(description = "I can add multiple products to the cart.")
    public void i_can_add_multiple_products_to_cart(){
        products.getFirstProduct();
        products.verifyProductInShoppingCartByName("Baby girl cardigan  was added to your shopping cart.");
        mainPage.resetPage();
        products.getThirdProduct();
        products.verifyProductInShoppingCartByName("Man Shoe was added to your shopping cart.");
        mainPage.resetPage();
    }

    @Feature("My Cart")
    @Test(description = "I can modify the quantity for a product in my cart")
    public void i_can_change_product_quantity_in_cart(){
        products.getFirstProduct();
        cart.modifyItemQuantity();
        cart.addQuantity("2");
        mainPage.resetPage();
    }

    @Feature("My Cart")
    @Test(description = "I am expecting to be able to remove item from my cart.")
    public void i_can_remove_item_from_cart(){
        products.getFirstProduct();
        products.verifyProductInShoppingCartByName("Baby girl cardigan  was added to your shopping cart.");
        cart.removeItemFromCart();
        mainPage.resetPage();
    }
    @Feature("My Wishlist")
    @Test(description = "I can remove an item from my wishlist.")
    public void i_can_remove_item_from_wishlist(){
        products.getSecondProduct();
        mainPage.email_address("Gicu@yahoo.com");
        mainPage.user_password("Gigel2000");
        mainPage.clickOnLoginButton();
        cart.removeItemFromWishlist();
        mainPage.userLogOut();
    }
    @Feature("Compare Products")
    @Test(description = "I can remove a product from compare list.")
    public void i_can_remove_from_compare_list_a_product(){
        products.getFifthProduct();
        mainPage.resetPage();
        mainPage.account_button();
        mainPage.my_account();
        mainPage.email_address("Gicu@yahoo.com");
        mainPage.user_password("Gigel2000");
        mainPage.clickOnLoginButton();
        cart.removeProductFromCompareList();
        mainPage.userLogOut();
        mainPage.resetPage();
    }
    @Feature("Compare Products")
    @Test(description = "I can compare a list of products.")
    public void i_can_compare_products(){
        products.getFifthProduct();
        mainPage.resetPage();
        products.getSixthProduct();
        mainPage.resetPage();
        mainPage.account_button();
        mainPage.my_account();
        mainPage.email_address("Gicu@yahoo.com");
        mainPage.user_password("Gigel2000");
        mainPage.clickOnLoginButton();
        cart.compareProducts();
        cart.removeProductFromCompareList();
        mainPage.userLogOut();
        mainPage.resetPage();
    }

    @Feature("Products")
    @Test(description = "I can select to view Accessories category.")
    public void category_list(){
        products.getCategory();
        mainPage.resetPage();
    }

    @Feature("Products")
    @Test(description = "I can select Accessories category by price.")
    public void select_products_by_price(){
        products.getCategory();
        products.getCategoryByPrice();
        mainPage.resetPage();
    }
    @Feature("Products")
    @Test(description = "I can select Accessories category by length.")
    public void select_product_by_length(){
        products.getCategory();
        sleep(4000);
        products.getCategoryByGender();
        sleep(4000);
        mainPage.resetPage();
    }
}
