package org.fasttrackit.magentotauproject;
import ClassData.ScreenShooter;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.$;

public class MainPage extends ScreenShooter {
    private SelenideElement accountButton = $(".skip-link.skip-account");
    private SelenideElement accountMenu = $("div#header-account li.first a[title*=Account]");
    private SelenideElement searchInput =  $("input#search.input-text");
    private SelenideElement emailAddress = $("input#email.input-text");
    private SelenideElement userPassword = $("input#pass.input-text");
    private SelenideElement loginButton = $("button#send2.button");
    private SelenideElement welcomeMessage = $("p.welcome-msg");
    private SelenideElement errorMessage = $(".error-msg");
    private SelenideElement guestMessage = $("p.welcome-msg");
    private SelenideElement setResetPage =$("a.logo");
    private SelenideElement logOut = $(".skip-content.skip-active li.last");
    private SelenideElement languageSelector = $("#select-language");
    private SelenideElement selectLanguage = $("#select-language:nth-child(2)");

    public void searchProduct(String productName){
        searchInput.click();
        searchInput.sendKeys(productName);
    }
    public void verifyWelcomeMessageByText(String welcomeText) {
        takeScreenShot("Verify welcome message text is " + welcomeText);
        welcomeMessage.shouldHave(exactText(welcomeText));
    }
    public void my_account(){
        accountMenu.click();
    }
    public void email_address(String emailAddress){
        this.emailAddress.click();
        this.emailAddress.sendKeys(emailAddress);

    }
    public void user_password(String userPassword){
        this.userPassword.click();
        this.userPassword.sendKeys(userPassword);
    }
    public void account_button(){
        accountButton.click();
        takeScreenShot("Click on Account button.");
    }
    public void clickOnLoginButton() {
        loginButton.click();
        takeScreenShot("Click on Login button.");
    }
    public void verifyErrorMessageByText(String errorText) {
    takeScreenShot("Verify error message text is " + errorMessage);
    errorMessage.shouldHave(exactText(errorText));
    }
    public void verifyGuestMessage(String guestText){
        takeScreenShot("Verify welcome message text is " + guestMessage);
        guestMessage.shouldHave(exactText(guestText));
    }
    public void resetPage(){
        setResetPage.click();
    }

    public void userLogOut(){
        accountButton.click();
        logOut.click();
    }
    public void languageSelection(){
        languageSelector.click();
        selectLanguage.selectOptionContainingText("French");
        takeScreenShot("Option for language selected is French");
    }
}
