package ClassData;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Cart extends Products{
    private SelenideElement editQuantity = $(".header-minicart");
    private SelenideElement changeQuantity = $(".product-cart-actions input");
    private SelenideElement submitQuantityButton = $("button.button.btn-update");
    private SelenideElement removeItem = $("td.product-cart-remove .btn-remove");
    private SelenideElement wishList = $("li.current");
    private SelenideElement wishListRemove = $("a.btn-remove.btn-remove2");
    private SelenideElement compareListRemove = $("a.btn-remove");
    private SelenideElement compareButton = $("div.actions .button span span");

    public void modifyItemQuantity(){
        changeQuantity.click();
        editQuantity.click();
    }

    public void addQuantity(String input){
        changeQuantity.clear();
        changeQuantity.sendKeys(input);
        submitQuantityButton.click();
    }
    public void removeItemFromCart(){
        removeItem.click();
        takeScreenShot("Item was removed from cart.");
    }
    public void removeItemFromWishlist(){
        wishList.click();
        wishListRemove.click();
        wishListRemove.pressEnter();
        takeScreenShot("Item was removed from my Wih List");
    }
    public void removeProductFromCompareList(){
        compareListRemove.click();
        compareListRemove.pressEnter();
    }
    public void compareProducts(){
        compareButton.click();
        takeScreenShot("I can compare this items");
    }
}
