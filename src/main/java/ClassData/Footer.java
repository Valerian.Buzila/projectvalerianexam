package ClassData;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Footer{
    private final ElementsCollection footerElements = $$("div.col-1 p");
    private SelenideElement footerInfo = $(".footer a[href*='about']");

    public void verifyFooterContent(){
        footerInfo.click();
        footerElements.shouldHave(CollectionCondition.size(2));
        String madisonIslandIs = "Madison Island is...";
        footerElements.get(0).shouldHave(Condition.text(madisonIslandIs));
        var strings = footerElements.get(1).text().split("\\n");
        madisonIslandIs = "...a breath of fresh air. ∞";
        strings[0].contains(madisonIslandIs);
        madisonIslandIs = "…a mecca for style savvy travellers. Ð";
        strings[1].contains(madisonIslandIs);
        madisonIslandIs = "…a curator of gorgeous sartorial design. ";
        strings[2].contains(madisonIslandIs);
        madisonIslandIs = "…a world class concept store.";
        strings[3].contains(madisonIslandIs);
    }
}
