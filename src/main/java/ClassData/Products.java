package ClassData;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;

public class Products extends ScreenShooter{
    private SelenideElement firstProduct =$("li.item.last:nth-child(1)");
    private SelenideElement secondProduct = $("li.item.last:nth-child(2)");
    private SelenideElement thirdProduct = $("li.item.last:nth-child(3)");
    private SelenideElement forthProduct = $("li.item.last:nth-child(4)");
    private SelenideElement fifthProduct = $("li.item.last:nth-child(5)");
    private SelenideElement addToCart = $("button.button.btn-cart:nth-child(1)");
    private SelenideElement shoppingCart = $(".success-msg");
    private SelenideElement wishList = $(".link-wishlist");
    private SelenideElement chelseaTee = $("li.product");
    private SelenideElement addToCompare = $("a.link-compare");
    private SelenideElement selectCategory = $(".level0.nav-3.parent");
    private SelenideElement selectByPrice = $("dd.even span.price");
    private SelenideElement selectByLength = $("dd.last.even");
    private SelenideElement verifyCategory = $("div.currently span.label");


    public void verifyProductInShoppingCartByName(String productName) {
        takeScreenShot("Verify product name is" + productName);
        shoppingCart.shouldHave(Condition.exactText(productName));
    }

    public void getFirstProduct() {
        firstProduct.click();
        addToCart.click();
    }

    public void getSecondProduct() {
        secondProduct.click();
        wishList.click();
    }

    public void getThirdProduct() {
        thirdProduct.click();
        addToCart.click();
    }

    public void getForthProduct(String productName) {
        forthProduct.click();
        takeScreenShot("Verify product name is" + productName);
        chelseaTee.shouldHave(Condition.exactText(productName));
    }

    public void getFifthProduct() {
        fifthProduct.click();
        addToCompare.click();
    }
    public void getSixthProduct(){
        firstProduct.click();
        addToCompare.click();
    }
    public void getCategory(){
        selectCategory.click();
        takeScreenShot("Accessories category selected");
    }
    public void getCategoryByPrice(){
        selectByPrice.click();
        takeScreenShot("Products from Accessories selected by price");
    }
    public void getCategoryByGender(){
        selectByLength.click();
        verifyCategory.shouldHave(Condition.exactText("Necklace Length:"));
        takeScreenShot("Products from Accessories selected by length");
    }
}
